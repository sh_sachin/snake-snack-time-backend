const express = require("express");

const router = express.Router();

const userController = require("../controller/userControllers");

//create new user if not exist
router.post("/", userController.create);

//update score of a player
router.put("/score", userController.updateScore);

//Retrieve top ten score of game
router.get("/topScores", userController.getTopTenScoreOfGame);

//Retrieve top ten score of a player
router.get("/topScores/:id", userController.getTopTenScore);

//Retrieve total game played
router.get("/gamePlayedPerDay", userController.totalGamePlayedPerDay);

//Retrieve total game of a single user with id
router.get("/gamePlayedPerDay/:id", userController.gamePlayedPerDay);

module.exports = router;
