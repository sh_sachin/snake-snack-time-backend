const express = require("express");
const cors = require("cors");

// create express app
const app = express();

// Setup server port
const port = process.env.PORT || 4000;

// parse requests of content-type - application/json
app.use(express.json());
app.use(cors());

// Configuring the database
const dbConfig = require("./config/database.config");
const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

// Connecting to the database
mongoose
  .connect(dbConfig.url, {
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Successfully connected to the database");
  })
  .catch((err) => {
    console.log("Could not connect to the database.", err);
    process.exit();
  });

//register schema for following models
require("./models/userModel");
require("./models/scoreModel");

//Require user routes
const userRoutes = require("./routes/userRoutes");

//using as middleware
app.use("/user", userRoutes);

// listen for requests
app.listen(port, () => {
  console.log(`Node server is listening on port ${port}`);
});
