const mongoose = require("mongoose");

const userSchema = mongoose.Schema(
  {
    userName: { type: String, require: true },
    gamePlayed: { type: Number, default: 0 },
    lastScore: { type: Number, default: 0 },
    topScore: { type: Number, default: 0 },
  },
  {
    timestamps: true,
  }
);

mongoose.model("user", userSchema);
