const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema.Types;

const scoreSchema = mongoose.Schema(
  {
    scoredBy: { type: ObjectId, ref: "user" },
    score: { type: Number, require: true },
  },
  {
    timestamps: true,
  }
);

mongoose.model("score", scoreSchema);
