async function verifyUser(req, res, next) {
  const { userName } = req.body;

  if (!userName) {
    return res.status(422).json({ message: "please enter all the fields!!!" });
  }
  //check Name
  User.findOne({ userName: userName })
    .then((user) => {})
    .catch((err) => {
      return req.status(422).json({ message: "User Not Found!!" });
    });
  const company_data_by_name = await verifySignUpDao.getCompanyDataByName(
    company_name
  );
  if (company_data_by_name.length > 0) {
    return res.status(422).json({ error: "Name is already taken!" });
  }
  next();
}

module.exports = verifyUser;
