const mongoose = require("mongoose");
const { use } = require("../routes/userRoutes");
const User = mongoose.model("user");
const Score = mongoose.model("score");

//Return user data if present in database otherwise create and send
exports.create = (req, res) => {
  const { userName } = req.body;

  if (!userName) {
    return res.status(404).json({ message: "please enter userName!!!" });
  }
  //check Name
  User.findOne({ userName: userName })
    .then((userDetail) => {
      if (userDetail) {
        return res.status(200).json(userDetail);
      }
      const user = new User({
        userName,
      });
      user
        .save()
        .then((userData) => {
          return res.status(200).json(userData);
        })
        .catch((err) => {
          return res
            .status(500)
            .json({ message: "Something went wrong while creating new user!" });
        });
    })
    .catch((err) => {
      return res.status(500).json({ message: "Something went wrong!!" });
    });
};

//will update score of a player
exports.updateScore = (req, res) => {
  const { _id, score } = req.body;

  if (!_id) {
    return res
      .status(404)
      .json({ message: "Please provide all the fields!!!" });
  }

  User.findById(_id)
    .then((userData) => {
      if (userData.topScore < score) {
        User.findByIdAndUpdate(
          _id,
          {
            $set: {
              topScore: score,
              lastScore: score,
              gamePlayed: userData.gamePlayed + 1,
            },
          },
          { new: true }
        ).exec((err, result) => {
          console.log(result, err);
          if (err) {
            return res.status(500).json({ message: "something went wrong!!" });
          }

          const newScore = new Score({
            scoredBy: result,
            score: score,
          });
          newScore
            .save()
            .then((scoreData) => {
              return res.status(201).json(scoreData);
            })
            .catch((err) => {
              return res.status(500).json({
                message: "Something went wrong while creating new user!",
              });
            });
        });
      } else {
        User.findByIdAndUpdate(
          _id,
          {
            $set: { lastScore: score, gamePlayed: userData.gamePlayed + 1 },
          },
          { new: true }
        ).exec((err, result) => {
          if (err) {
            return res.status(500).json({ message: "something went wrong!!" });
          }
          const newScore = new Score({
            scoredBy: result,
            score: score,
          });
          newScore
            .save()
            .then((scoreData) => {
              return res.status(200).json(scoreData);
            })
            .catch((err) => {
              return res.status(500).json({
                message: "Something went wrong while creating new user!",
              });
            });
        });
      }
    })
    .catch((err) => {
      return res.status(422).json({ err });
    });
};

//fetch top ten score of a player
exports.getTopTenScore = (req, res) => {
  const { id: _id } = req.params;

  if (!_id) {
    return res
      .status(404)
      .json({ message: "please provide the required fields!!!" });
  }
  Score.find({ scoredBy: _id })
    .populate("scoredBy", "userName")
    .sort({ score: -1 })
    .limit(10)
    .then((data) => {
      return res.status(200).json(data);
    })
    .catch((err) => {
      return res.status(400).json({ message: "something went wrong!!!!" });
    });
};

//fetch overall top ten score of game
exports.getTopTenScoreOfGame = (req, res) => {
  Score.find({})
    .populate("scoredBy", "userName")
    .sort({ score: -1 })
    .limit(10)
    .then((scoreData) => {
      return res.status(200).json(scoreData);
    })
    .catch((error) => {
      return res.status(500).json({ message: "something went wrong" });
    });
};

exports.findOne = (req, res) => {
  User.findById(req.params.id)
    .then((user) => {
      if (!user) {
        return res.status(404).send({
          message: `User not found with id ${req.params.id}`,
        });
      }
      res.send(user);
    })
    .catch((err) => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "USer not found",
        });
      }
      return res.status(500).send({
        message: "Error getting user data",
      });
    });
};

//get total game played by a player
exports.gamePlayedPerDay = (req, res) => {
  const { id: _id } = req.params;

  if (!_id) {
    return res
      .status(404)
      .json({ message: "Please provide all the fields!!!" });
  }
  console.log(_id);
  Score.find({ scoredBy: _id })
    .then((scoreData) => {
      console.log("hello", scoreData);
      return res.status(200).json({ totalGames: scoreData.length });
    })
    .catch((err) => {
      return res.status(500).json({ message: "something went wrong!!!!" });
    });
};

exports.totalGamePlayedPerDay = (req, res) => {
  Score.find()
    .then((scoreData) => {
      console.log("hello", scoreData);
      return res.status(200).json({ totalGames: scoreData.length });
    })
    .catch((err) => {
      return res.status(500).json({ message: "something went wrong!!!!" });
    });
};
